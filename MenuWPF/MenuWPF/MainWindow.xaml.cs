﻿using System.IO;
using System.Text.RegularExpressions;
using System.Windows;

namespace MenuWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Domain _domain;

        public MainWindow()
        {
            InitializeComponent();
            _domain = new Domain();
        }

        private void Load(bool isLoading)
        {
            if (isLoading)
            {
                calculateButton.IsEnabled = downloadButton.IsEnabled = false;
                progressBarResult.Visibility = Visibility.Visible;
                progressResultText.Text = "Loading...";
            }
            else
            {
                calculateButton.IsEnabled = downloadButton.IsEnabled = true;
                progressBarResult.Visibility = Visibility.Collapsed;
                progressResultText.Text = "Done.";
            }
        }

        private async void CalculateButtonClick(object sender, RoutedEventArgs e)
        {
            Load(true);

            await _domain.ExecuteAssemblyAsync("CalculatorApp", "CalculatorApp.exe", null);

            Load(false);

            try
            {
                string path = @"C:\ExpressionResult\result.txt";
                using (var stream = new StreamReader(path))
                {
                    MessageBox.Show($"Result: {stream.ReadLine()}");
                }
            }
            catch
            {
                MessageBox.Show("Error! Couldn't get calculation's result! Please try again!");
            }
        }

        private async void DownloadButtonClick(object sender, RoutedEventArgs e)
        {
            var fullPath = filePathText.Text;

            if (fullPath == "")
            {
                MessageBox.Show("Enter a file path!");
                return;
            }

            if (!Regex.IsMatch(fullPath, @"^(?:[\a-zA-Z]\:|\\)(\\[a-zA-Z_\-\s0-9\.]+)+\.(bin)$"))
            {
                MessageBox.Show("The file path's incorrect!");
                return;
            }

            Load(true);
            
            await _domain.ExecuteAssemblyAsync("DownloaderApp", "DownloaderApp.exe", new string[] { fullPath });

            Load(false);

            MessageBox.Show((File.Exists(fullPath)) ? "File was downloaded successfully!" : "Error! File couldn't be downloaded! Please check your file path!");
        }
    }
}
